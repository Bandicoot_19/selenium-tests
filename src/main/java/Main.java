import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import pages.Gender;
import pages.RegistrationPage;

import java.awt.*;

public class Main {

    public static void main(String[] args) throws InterruptedException, AWTException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\mnady\\Desktop\\webDriver\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        Actions actions = new Actions(driver);
        RegistrationPage loginPage = new RegistrationPage(driver, actions);
        driver.get("http://frontend.nspk-aws-silver.luxoft.com/register/");
        loginPage.login("9139872479", "2222111120033983");
        loginPage.inputSmsCode("1111");
        loginPage.register("Надыкто Михаил Олегович", "mnadykto@list.com", "Bolk4400", "01.01.2000", Gender.FEMALE);
    }
}
