package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.time.Duration;

public class RegistrationPage {
    WebDriver driver;
    Actions actions;

    public RegistrationPage(WebDriver driver, Actions actions) {
        this.driver = driver;
        this.actions = actions;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public Actions getActions() {
        return actions;
    }

    public void setActions(Actions actions) {
        this.actions = actions;
    }

    public void login(String phoneNumber, String panCard) {
        WebElement phoneInput = (new WebDriverWait(driver, Duration.ofSeconds(5))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='login']"))));
        WebElement cardInput = driver.findElement(By.xpath("//input[@name='panCard']"));
        WebElement approve1 = driver.findElement(By.xpath("//label[@for='approve1']//span[@class='checkbox__bg']"));
        WebElement approve2 = driver.findElement(By.xpath("//label[@for='approve2']//span[@class='checkbox__bg']"));
        WebElement buttonContinue = driver.findElement(By.xpath("//div[@class='reg__footer']//button"));
        // У button нет атрибутов id или name, в след. функции это создает неудобство
        phoneInput.click(); // этим я обхожу сдвиг вправо
        phoneInput.sendKeys(phoneNumber);
        cardInput.click();
        cardInput.sendKeys(panCard);
        actions.moveToElement(approve1).click().perform(); // span не кликабельный, наводим на него и кликаем
        actions.moveToElement(approve2).click().perform();
        buttonContinue.click();
    }

    public void inputSmsCode(String code) {
        WebElement codeInput = (new WebDriverWait(driver, Duration.ofSeconds(5))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='code']"))));
        WebElement buttonContinue = driver.findElement(By.xpath("//div[@class='reg__footer']//button//span[text()='Продолжить']")); // уже не так определенно
        codeInput.click();
        codeInput.sendKeys(code);
        actions.moveToElement(buttonContinue).click().perform();
    }

    public void register(String name, String email, String password, String birthday, Gender gender) throws InterruptedException, AWTException {
        WebElement nameInput = (new WebDriverWait(driver, Duration.ofSeconds(60)) // пока что длительное ожидания после ввода смс кода
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='name']"))));
        WebElement emailInput = driver.findElement(By.xpath("//input[@name='email']"));
        WebElement passwordInput = driver.findElement(By.xpath("//input[@name='pass']"));
        WebElement repeatPasswordInput = driver.findElement(By.xpath("//input[@name='pass_confirmation']"));
        WebElement birthdayInput = driver.findElement(By.xpath("//input[@name='birth']"));
        WebElement maleButton = driver.findElement(By.xpath("//label[@for='male']"));
        WebElement femaleButton = driver.findElement(By.xpath("//label[@for='female']"));
        WebElement registrationButton = driver.findElement(By.xpath("//div[@class='reg__footer']//button"));

        nameInput.sendKeys(name);
        emailInput.sendKeys(email);
        passwordInput.sendKeys(password);
        repeatPasswordInput.sendKeys(password);
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,300)", "");
        birthdayInput.click();
        birthdayInput.sendKeys(birthday, Keys.ENTER);
        switch (gender) {
            case MALE:
                actions.moveToElement(maleButton).moveByOffset(-3,-3).click().perform();
                break;
            case FEMALE:
                actions.moveToElement(femaleButton).moveByOffset(-3,-3).click().perform();
                break;
        }
        registrationButton.click();
    }
}

